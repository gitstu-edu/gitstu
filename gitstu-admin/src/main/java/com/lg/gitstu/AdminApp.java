package com.lg.gitstu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBoot方式启动类
 *
 * @author 迅白旺旺
 * @Date 2017/5/21 12:06
 */
@SpringBootApplication
public class AdminApp {

    private final static Logger logger = LoggerFactory.getLogger(AdminApp.class);

    public static void main(String[] args) {
        SpringApplication.run(AdminApp.class, args);
        logger.info("Application is success!");
    }
}
