package com.lg.gitstu.admin.common.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;

/**
 * 待办
 * @author 迅白旺旺
 * @date 2018/7/15 10:49
 */
@TableName("sys_backlog")
@Data
public class Backlog extends Model<Backlog> {
    /**
     * 主键
     */
    @TableId(value="id", type= IdType.AUTO)
    private Long id;

    /**
     * 描述
     */
    private String desc;

    /**
     * 状态
     */
    private String status;

    @Override
    protected Serializable pkVal() {
        return id;
    }
}
