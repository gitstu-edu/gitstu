package com.lg.gitstu.admin.common.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 迅白旺旺
 * @date 2018/7/15 11:00
 */
@TableName("sys_message")
@Data
public class Message extends Model<Message> {

    /**
     * 主键
     */
    @TableId(value="id", type= IdType.AUTO)
    private Long id;

    /**
     * 消息内容
     */
    private String content;

    /**
     * 状态
     */
    private String status;

    @Override
    protected Serializable pkVal() {
        return id;
    }
}
