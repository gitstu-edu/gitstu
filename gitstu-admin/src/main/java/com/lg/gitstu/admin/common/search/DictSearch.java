package com.lg.gitstu.admin.common.search;

import com.lg.gitstu.core.base.BaseSearch;
import lombok.Data;

@Data
public class DictSearch extends BaseSearch {
    private String name;
    private String code;
}
