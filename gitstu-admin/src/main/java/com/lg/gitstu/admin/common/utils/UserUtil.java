package com.lg.gitstu.admin.common.utils;

import com.lg.gitstu.admin.common.model.User;
import com.lg.gitstu.admin.common.dto.UserDto;
import org.springframework.beans.BeanUtils;

/**
 * 用户创建工厂
 *
 * @author 迅白旺旺
 * @date 2017-05-05 22:43
 */
public class UserUtil {

    public static User createUser(UserDto userDto){
        if(userDto == null){
            return null;
        }else{
            User user = new User();
            BeanUtils.copyProperties(userDto,user);
            return user;
        }
    }
}
