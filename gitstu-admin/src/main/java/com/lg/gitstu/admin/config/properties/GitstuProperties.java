package com.lg.gitstu.admin.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.File;

import static com.lg.gitstu.core.support.StrKit.isEmpty;
import static com.lg.gitstu.core.util.ToolUtil.getTempPath;

/**
 * gitstu项目配置
 *
 * @author 迅白旺旺
 * @Date 2017/5/23 22:31
 */
@Component("gitstuProperties")
@ConfigurationProperties(prefix = GitstuProperties.PREFIX)
public class GitstuProperties {

    public static final String PREFIX = "gitstu";

    @Getter
    @Setter
    private Boolean kaptchaOpen = false;

    @Getter
    @Setter
    private Boolean swaggerOpen = false;

    @Getter
    @Setter
    private String fileUploadPath;

    @Getter
    @Setter
    private Boolean haveCreatePath = false;

    @Getter
    @Setter
    private Boolean springSessionOpen = false;

    /**
     * session 失效时间（默认为30分钟 单位：秒）
     */
    @Getter
    @Setter
    private Integer sessionInvalidateTime = 30 * 60;

    /**
     * session 验证失效时间（默认为15分钟 单位：秒）
     */
    @Getter
    @Setter
    private Integer sessionValidationInterval = 15 * 60;
}
