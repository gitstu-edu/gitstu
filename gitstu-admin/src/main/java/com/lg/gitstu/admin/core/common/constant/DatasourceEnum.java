package com.lg.gitstu.admin.core.common.constant;

/**
 * 
 * 多数据源的枚举
 *
 * @author 迅白旺旺
 * @date 2017年3月5日 上午10:15:02
 */
public interface DatasourceEnum {

	String DATA_SOURCE_GITSTU = "dataSourceGitstu";			//gitstu数据源
	
	String DATA_SOURCE_BIZ = "dataSourceBiz";			//其他业务的数据源
}
