package com.lg.gitstu.admin.core.common.constant.dictmap.base;

/**
 * 系统相关的字典
 *
 * @author 迅白旺旺
 * @date 2017-05-06 15:48
 */
public class SystemDict extends AbstractDictMap {

    @Override
    public void init() {

    }

    @Override
    protected void initBeWrapped() {

    }
}
