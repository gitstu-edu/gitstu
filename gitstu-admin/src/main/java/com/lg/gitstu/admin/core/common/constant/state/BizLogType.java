package com.lg.gitstu.admin.core.common.constant.state;

import lombok.Getter;
import lombok.Setter;

/**
 * 业务日志类型
 *
 * @author 迅白旺旺
 * @Date 2017年1月22日 下午12:14:59
 */
public enum BizLogType {

    ALL(0, null),//全部日志
    BUSSINESS(1, "业务日志"),
    EXCEPTION(2, "异常日志");

    @Getter
    @Setter
    Integer val;

    @Getter
    @Setter
    String message;

    BizLogType(Integer val, String message) {
        this.val = val;
        this.message = message;
    }

    public static String valueOf(Integer value) {
        if (value == null) {
            return null;
        } else {
            for (BizLogType bizLogType : BizLogType.values()) {
                if (bizLogType.getVal().equals(value)) {
                    return bizLogType.getMessage();
                }
            }
            return null;
        }
    }
}
