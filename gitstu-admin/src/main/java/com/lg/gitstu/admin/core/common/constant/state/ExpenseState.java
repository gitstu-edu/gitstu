package com.lg.gitstu.admin.core.common.constant.state;

import lombok.Getter;
import lombok.Setter;

/**
 * 是否是菜单的枚举
 *
 * @author 迅白旺旺
 * @date 2017年6月1日22:50:11
 */
public enum ExpenseState {

    SUBMITING(1, "待提交"),
    CHECKING(2, "待审核"),
    PASS(3, "审核通过"),
    UN_PASS(4, "未通过");

    @Getter
    @Setter
    int code;

    @Getter
    @Setter
    String message;

    ExpenseState(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static String valueOf(Integer status) {
        if (status == null) {
            return "";
        } else {
            for (ExpenseState s : ExpenseState.values()) {
                if (s.getCode() == status) {
                    return s.getMessage();
                }
            }
            return "";
        }
    }
}
