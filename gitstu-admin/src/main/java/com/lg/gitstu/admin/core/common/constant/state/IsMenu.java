package com.lg.gitstu.admin.core.common.constant.state;

import lombok.Getter;
import lombok.Setter;

/**
 * 是否是菜单的枚举
 *
 * @author 迅白旺旺
 * @date 2017年6月1日22:50:11
 */
public enum IsMenu {

    YES(1, "是"),
    NO(0, "不是");//不是菜单的是按钮

    @Getter
    @Setter
    int code;

    @Getter
    @Setter
    String message;

    IsMenu(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static String valueOf(Integer status) {
        if (status == null) {
            return "";
        } else {
            for (IsMenu s : IsMenu.values()) {
                if (s.getCode() == status) {
                    return s.getMessage();
                }
            }
            return "";
        }
    }
}
