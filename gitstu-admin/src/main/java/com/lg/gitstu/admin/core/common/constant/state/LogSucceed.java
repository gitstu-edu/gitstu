package com.lg.gitstu.admin.core.common.constant.state;

import lombok.Getter;
import lombok.Setter;

/**
 * 业务是否成功的日志记录
 *
 * @author 迅白旺旺
 * @Date 2017年1月22日 下午12:14:59
 */
public enum LogSucceed {

    SUCCESS("成功"),
    FAIL("失败");

    @Getter
    @Setter
    String message;

    LogSucceed(String message) {
        this.message = message;
    }
}
