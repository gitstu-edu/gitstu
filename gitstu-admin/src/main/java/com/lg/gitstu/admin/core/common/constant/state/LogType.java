package com.lg.gitstu.admin.core.common.constant.state;

import lombok.Getter;
import lombok.Setter;

/**
 * 日志类型
 *
 * @author 迅白旺旺
 * @Date 2017年1月22日 下午12:14:59
 */
public enum LogType {

    LOGIN("登录日志"),
    LOGIN_FAIL("登录失败日志"),
    EXIT("退出日志"),
    EXCEPTION("异常日志"),
    BUSSINESS("业务日志");

    @Getter
    @Setter
    String message;

    LogType(String message) {
        this.message = message;
    }
}
