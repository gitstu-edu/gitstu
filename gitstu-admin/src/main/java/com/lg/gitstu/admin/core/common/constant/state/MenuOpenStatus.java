package com.lg.gitstu.admin.core.common.constant.state;

import lombok.Getter;
import lombok.Setter;

/**
 * 菜单是否打开的状态
 *
 * @author 迅白旺旺
 * @Date 2017年4月8日10:12:15
 */
public enum MenuOpenStatus {

    OPEN(1, "打开"),
    CLOSE(0, "关闭");

    @Getter
    @Setter
    int code;

    @Getter
    @Setter
    String message;

    MenuOpenStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static String valueOf(Integer status) {
        if (status == null) {
            return "";
        } else {
            for (MenuOpenStatus s : MenuOpenStatus.values()) {
                if (s.getCode() == status) {
                    return s.getMessage();
                }
            }
            return "";
        }
    }
}
