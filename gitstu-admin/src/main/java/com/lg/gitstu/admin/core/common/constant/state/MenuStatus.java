package com.lg.gitstu.admin.core.common.constant.state;

import lombok.Getter;
import lombok.Setter;

/**
 * 菜单的状态
 *
 * @author 迅白旺旺
 * @Date 2017年1月22日 下午12:14:59
 */
public enum MenuStatus {

    ENABLE(1, "启用"),
    DISABLE(0, "禁用");

    @Getter
    @Setter
    int code;

    @Getter
    @Setter
    String message;

    MenuStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static String valueOf(Integer status) {
        if (status == null) {
            return "";
        } else {
            for (MenuStatus s : MenuStatus.values()) {
                if (s.getCode() == status) {
                    return s.getMessage();
                }
            }
            return "";
        }
    }
}
