package com.lg.gitstu.admin.core.common.constant.state;

import lombok.Getter;
import lombok.Setter;

/**
 * 数据库排序
 *
 * @author 迅白旺旺
 * @Date 2017年5月31日20:48:41
 */
public enum Order {

    ASC("asc"), DESC("desc");

    @Getter
    @Setter
    private String des;

    Order(String des) {
        this.des = des;
    }
}
