package com.lg.gitstu.admin.core.common.exception;

/**
 * 验证码错误异常
 *
 * @author 迅白旺旺
 * @date 2017-05-05 23:52
 */
public class InvalidKaptchaException extends RuntimeException {
}
