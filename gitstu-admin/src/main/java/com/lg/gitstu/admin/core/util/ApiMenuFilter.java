package com.lg.gitstu.admin.core.util;

import com.lg.gitstu.admin.config.properties.GitstuProperties;
import com.lg.gitstu.admin.core.common.constant.Const;
import com.lg.gitstu.core.node.MenuNode;
import com.lg.gitstu.core.util.SpringContextHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * api接口文档显示过滤
 *
 * @author 迅白旺旺
 * @date 2017-08-17 16:55
 */
public class ApiMenuFilter extends MenuNode {

    public static List<MenuNode> build(List<MenuNode> nodes) {

        //如果关闭了接口文档,则不显示接口文档菜单
        GitstuProperties gitstuProperties = SpringContextHolder.getBean(GitstuProperties.class);
        if (!gitstuProperties.getSwaggerOpen()) {
            List<MenuNode> menuNodesCopy = new ArrayList<>();
            for (MenuNode menuNode : nodes) {
                if (Const.API_MENU_NAME.equals(menuNode.getName())) {
                    continue;
                } else {
                    menuNodesCopy.add(menuNode);
                }
            }
            nodes = menuNodesCopy;
        }

        return nodes;
    }
}
