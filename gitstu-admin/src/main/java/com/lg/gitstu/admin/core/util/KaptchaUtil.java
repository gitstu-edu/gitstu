package com.lg.gitstu.admin.core.util;

import com.lg.gitstu.admin.config.properties.GitstuProperties;
import com.lg.gitstu.core.util.SpringContextHolder;

/**
 * 验证码工具类
 */
public class KaptchaUtil {

    /**
     * 获取验证码开关
     */
    public static Boolean getKaptchaOnOff() {
        return SpringContextHolder.getBean(GitstuProperties.class).getKaptchaOpen();
    }
}