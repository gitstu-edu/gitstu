package com.lg.gitstu.admin.modules.system.controller;

import com.google.code.kaptcha.Constants;
import com.lg.gitstu.admin.core.common.exception.InvalidKaptchaException;
import com.lg.gitstu.admin.core.log.LogManager;
import com.lg.gitstu.admin.core.log.factory.LogTaskFactory;
import com.lg.gitstu.admin.core.shiro.ShiroKit;
import com.lg.gitstu.admin.core.shiro.ShiroUser;
import com.lg.gitstu.admin.core.util.ApiMenuFilter;
import com.lg.gitstu.admin.core.util.KaptchaUtil;
import com.lg.gitstu.admin.common.model.Backlog;
import com.lg.gitstu.admin.common.model.Message;
import com.lg.gitstu.admin.common.model.Notice;
import com.lg.gitstu.admin.common.model.User;
import com.lg.gitstu.admin.modules.system.service.IMenuService;
import com.lg.gitstu.admin.modules.system.service.IUserService;
import com.lg.gitstu.core.base.controller.BaseController;
import com.lg.gitstu.core.node.MenuNode;
import com.lg.gitstu.core.util.ToolUtil;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import static com.lg.gitstu.core.support.HttpKit.getIp;

/**
 * 登录控制器
 *
 * @author 迅白旺旺
 * @Date 2017年1月10日 下午8:25:24
 */
@Controller
public class LoginController extends BaseController {

    @Autowired
    private IMenuService menuService;

    @Autowired
    private IUserService userService;

    /**
     * 跳转到主页
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        //获取菜单列表
        List<Integer> roleList = ShiroKit.getUser().getRoleList();
        if (roleList == null || roleList.size() == 0) {
            ShiroKit.getSubject().logout();
            model.addAttribute("tips", "该用户没有角色，无法登陆");
            return "/login.html";
        }
        List<MenuNode> menus = menuService.getMenusByRoleIds(roleList);
        List<MenuNode> titles = MenuNode.buildTitle(menus);
        titles = ApiMenuFilter.build(titles);

        //获取用户头像、昵称
        Integer id = ShiroKit.getUser().getId();
        User user = userService.selectById(id);
        String avatar = user.getAvatar();

        //todo 模拟数据，需要修改
        //面包屑导航
        List<MenuNode> breadcrumbs = new ArrayList();
        MenuNode mn1 = new MenuNode();
        mn1.setName("父菜单");
        mn1.setUrl("http://baidu.com");
        MenuNode mn2 = new MenuNode();
        mn2.setName("test");

        breadcrumbs.add(mn1);
        breadcrumbs.add(mn2);

        //待办
        List<Backlog> backlogs = new ArrayList();
        Backlog bl1 = new Backlog();
        bl1.setDesc("待办11111111111");
        Backlog bl2 = new Backlog();
        bl2.setDesc("待办22222222222");

        backlogs.add(bl1);
        backlogs.add(bl2);

        //通知
        List<Notice> notices = new ArrayList();
        Notice n1 = new Notice();
        n1.setContent("111111111111");
        Notice n2 = new Notice();
        n2.setContent("222222222222");

        notices.add(n1);
        notices.add(n2);

        //消息
        List<Message> messages = new ArrayList();
        Message m1 = new Message();
        m1.setContent("111111111111");
        Message m2 = new Message();
        m2.setContent("222222222222");

        messages.add(m1);
        messages.add(m2);

        //放入session
        HttpSession session = getSession();
        session.setAttribute("avatar",avatar);//头像
        session.setAttribute("name",user.getName());//姓名
        session.setAttribute("titles",titles);//菜单
        session.setAttribute("backlogs",backlogs);//待办
        session.setAttribute("notices",notices);//通知
        session.setAttribute("messages",messages);//消息

        model.addAttribute("breadcrumb","仪表盘");
        return "/index.html";
    }

    /**
     * 跳转到登录页面
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        if (ShiroKit.isAuthenticated() || ShiroKit.getUser() != null) {
            return REDIRECT + "/";
        } else {
            return "/login.html";
        }
    }

    /**
     * 点击登录执行的动作
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String loginVali() {

        String username = super.getPara("username").trim();
        String password = super.getPara("password").trim();
        String remember = super.getPara("remember");

        //验证验证码是否正确
        if (KaptchaUtil.getKaptchaOnOff()) {
            String kaptcha = super.getPara("kaptcha").trim();
            String code = (String) super.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
            if (ToolUtil.isEmpty(kaptcha) || !kaptcha.equalsIgnoreCase(code)) {
                throw new InvalidKaptchaException();
            }
        }

        Subject currentUser = ShiroKit.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password.toCharArray());

        if ("on".equals(remember)) {
            token.setRememberMe(true);
        } else {
            token.setRememberMe(false);
        }

        currentUser.login(token);

        ShiroUser shiroUser = ShiroKit.getUser();
        super.getSession().setAttribute("shiroUser", shiroUser);
        super.getSession().setAttribute("username", shiroUser.getAccount());

        LogManager.me().executeLog(LogTaskFactory.loginLog(shiroUser.getId(), getIp()));

        ShiroKit.getSession().setAttribute("sessionFlag", true);

        return REDIRECT + "/";
    }

    /**
     * 退出登录
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logOut() {
        LogManager.me().executeLog(LogTaskFactory.exitLog(ShiroKit.getUser().getId(), getIp()));
        ShiroKit.getSubject().logout();
        return REDIRECT + "/login";
    }
}
