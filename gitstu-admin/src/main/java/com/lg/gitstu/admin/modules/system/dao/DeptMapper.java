package com.lg.gitstu.admin.modules.system.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lg.gitstu.admin.common.model.Dept;
import com.lg.gitstu.core.node.ZTreeNode;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author 迅白旺旺
 * @since 2017-07-11
 */
public interface DeptMapper extends BaseMapper<Dept> {

    /**
     * 获取ztree的节点列表
     */
    List<ZTreeNode> tree();

    /**
     * 获取所有部门列表
     */
    List<Map<String, Object>> list(@Param("condition") String condition);

}