package com.lg.gitstu.admin.modules.system.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lg.gitstu.admin.common.model.Expense;

/**
 * <p>
  * 报销表 Mapper 接口
 * </p>
 *
 * @author 迅白旺旺
 * @since 2017-12-04
 */
public interface ExpenseMapper extends BaseMapper<Expense> {

}