package com.lg.gitstu.admin.modules.system.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lg.gitstu.admin.common.model.Relation;

/**
 * <p>
  * 角色和菜单关联表 Mapper 接口
 * </p>
 *
 * @author 迅白旺旺
 * @since 2017-07-11
 */
public interface RelationMapper extends BaseMapper<Relation> {

}