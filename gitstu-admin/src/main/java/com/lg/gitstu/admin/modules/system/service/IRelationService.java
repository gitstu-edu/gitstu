package com.lg.gitstu.admin.modules.system.service;

import com.baomidou.mybatisplus.service.IService;
import com.lg.gitstu.admin.common.model.Relation;

/**
 * <p>
 * 角色和菜单关联表 服务类
 * </p>
 *
 * @author 迅白旺旺
 * @since 2018-02-22
 */
public interface IRelationService extends IService<Relation> {

}
