package com.lg.gitstu.admin.modules.system.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lg.gitstu.admin.modules.system.dao.RelationMapper;
import com.lg.gitstu.admin.common.model.Relation;
import com.lg.gitstu.admin.modules.system.service.IRelationService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色和菜单关联表 服务实现类
 * </p>
 *
 * @author 迅白旺旺
 * @since 2018-02-22
 */
@Service
public class RelationServiceImpl extends ServiceImpl<RelationMapper, Relation> implements IRelationService {

}
