/**
 * 公共业务函数处理
 *
 * @author 迅白旺旺
 */
jQuery(function($) {
    /*搜索模式切换*/
    $('#gs-search-type').click(function () {
        if(GsUtils.cbChecked(this)){
            $('#gs-search-title').html('简易搜索');
            $('#gs-search-advanced').addClass('hide');
            $('#gs-search-easy').removeClass('hide');
        }else{
            $('#gs-search-title').html('高级搜索');
            $('#gs-search-advanced').removeClass('hide');
            $('#gs-search-easy').addClass('hide');
        }
    });
});