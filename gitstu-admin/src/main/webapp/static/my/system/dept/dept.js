/**
 * 部门管理管理初始化
 */
var Dept = {
    id: "DeptTable",	//表格id
    seItem: null,		//选中的条目
    table: null
};

/**
 * 初始化表格的列
 */
Dept.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '主键id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '排序', field: 'num', visible: true, align: 'center', valign: 'middle'},
            {title: '父部门id', field: 'pid', visible: true, align: 'center', valign: 'middle'},
            {title: '父级ids', field: 'pids', visible: true, align: 'center', valign: 'middle'},
            {title: '简称', field: 'simplename', visible: true, align: 'center', valign: 'middle'},
            {title: '全称', field: 'fullname', visible: true, align: 'center', valign: 'middle'},
            {title: '提示', field: 'tips', visible: true, align: 'center', valign: 'middle'},
            {title: '版本（乐观锁保留字段）', field: 'version', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Dept.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        GsUtils.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Dept.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加部门管理
 */
Dept.add = function () {
    location.href = GsUtils.ctxPath + '/dept/dept_add';
};

/**
 * 打开查看部门管理详情
 */
Dept.edit = function () {
    if (this.check()) {
        location.href = GsUtils.ctxPath + '/dept/dept_edit/' + Dept.seItem.id;
    }
};

/**
 * 删除部门管理
 */
Dept.delete = function () {
    if (this.check()) {
        var operation = function(){
            var ajax = new $ax(GsUtils.ctxPath + "/dept/delete", function (data) {
                GsUtils.success("删除成功!");
                Dept.table.refresh();
            }, function (data) {
                GsUtils.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("deptId",Dept.seItem.id);
            ajax.start();
        };

        GsUtils.confirm("是否刪除部门管理 " + Dept.seItem.name + "?", operation);
    }
};

/**
 * 查询部门管理列表
 */
Dept.search = function () {
    var queryData = {};
    if(GsUtils.cbChecked($('#gs-search-type'))){
        queryData['gsCondition'] = $("#gs-search-easy").find('input[name="gsCondition"]').val();
    }else{
        queryData['name'] = $("#gs-search-advanced").find('input[name="name"]').val();
        queryData['code'] = $("#gs-search-advanced").find('input[name="code"]').val();
    }
    Dept.table.refresh({query: queryData});
};

/**
 * 重置部门管理列表
 */
Dept.resetSearch = function(){
    Dept.table.setQueryParams({});
    Dept.table.refresh();
};

$(function () {
    var defaultColunms = Dept.initColumn();
    var table = new GSTable(Dept.id, "/dept/list", defaultColunms);
    table.setPaginationType("client");
    Dept.table = table.init();
});
