/**
 * 字典管理初始化
 */
var Dict = {
    id: "DictTable",	//表格id
    seItem: null,		//选中的条目
    table: null
};

/**
 * 初始化表格的列
 */
Dict.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '名称', field: 'name', align: 'center', valign: 'middle', sortable: true},
        {title: '详情', field: 'detail', align: 'center', valign: 'middle', sortable: true},
        {title: '备注', field: 'tips', align: 'center', valign: 'middle', sortable: true}];
};

/**
 * 检查是否选中
 */
Dict.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        GsUtils.info("请先选中表格中的某一记录！");
        return false;
    } else {
        Dict.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加字典
 */
Dict.add = function () {
    location.href = GsUtils.ctxPath + '/dict/dict_add';
};

/**
 * 打开查看字典详情
 */
Dict.update = function () {
    if (this.check()) {
        location.href = GsUtils.ctxPath + '/dict/dict_edit/' + Dict.seItem.id;
    }
};

/**
 * 删除字典
 */
Dict.delete = function () {
    if (this.check()) {

        var operation = function(){
            var ajax = new $ax(GsUtils.ctxPath + "/dict/delete", function (data) {
                GsUtils.success("删除成功!");
                Dict.table.refresh();
            }, function (data) {
                GsUtils.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("dictId", Dict.seItem.id);
            ajax.start();
        };

        GsUtils.confirm("是否刪除字典 " + Dict.seItem.name + "?", operation);
    }
};

/**
 * 查询字典列表
 */
Dict.search = function () {
    var queryData = {};
    if(GsUtils.cbChecked($('#gs-search-type'))){
        queryData['gsCondition'] = $("#gs-search-easy").find('input[name="gsCondition"]').val();
    }else{
        queryData['name'] = $("#gs-search-advanced").find('input[name="name"]').val();
        queryData['code'] = $("#gs-search-advanced").find('input[name="code"]').val();
    }
    Dict.table.refresh({query: queryData});
};

/**
 * 重置字典列表
 */
Dict.resetSearch = function(){
    Dict.table.setQueryParams({});
    Dict.table.refresh();
};

$(function () {
    var defaultColunms = Dict.initColumn();
    var table = new GSTable(Dict.id, "/dict/list", defaultColunms);
    table.setPaginationType("client");
    Dict.table = table.init();
});
