/**
 * 登录日志管理初始化
 */
var LoginLog = {
    id: "LoginLogTable",	//表格id
    seItem: null,		//选中的条目
    table: null
};

/**
 * 初始化表格的列
 */
LoginLog.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '主键', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '日志名称', field: 'logname', visible: true, align: 'center', valign: 'middle'},
            {title: '管理员id', field: 'userid', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'},
            {title: '是否执行成功', field: 'succeed', visible: true, align: 'center', valign: 'middle'},
            {title: '具体消息', field: 'message', visible: true, align: 'center', valign: 'middle'},
            {title: '登录ip', field: 'ip', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
LoginLog.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        GsUtils.info("请先选中表格中的某一记录！");
        return false;
    }else{
        LoginLog.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加登录日志
 */
LoginLog.add = function () {
    location.href = GsUtils.ctxPath + '/loginLog/loginLog_add';
};

/**
 * 打开查看登录日志详情
 */
LoginLog.edit = function () {
    if (this.check()) {
        location.href = GsUtils.ctxPath + '/loginLog/loginLog_edit/' + LoginLog.seItem.id;
    }
};

/**
 * 删除登录日志
 */
LoginLog.delete = function () {
    if (this.check()) {
        var operation = function(){
            var ajax = new $ax(GsUtils.ctxPath + "/loginLog/delete", function (data) {
                GsUtils.success("删除成功!");
                LoginLog.table.refresh();
            }, function (data) {
                GsUtils.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("loginLogId",LoginLog.seItem.id);
            ajax.start();
        };

        GsUtils.confirm("是否刪除登录日志 " + LoginLog.seItem.name + "?", operation);
    }
};

/**
 * 查询登录日志列表
 */
LoginLog.search = function () {
    var queryData = {};
    if(GsUtils.cbChecked($('#gs-search-type'))){
        queryData['gsCondition'] = $("#gs-search-easy").find('input[name="gsCondition"]').val();
    }else{
        queryData['name'] = $("#gs-search-advanced").find('input[name="name"]').val();
        queryData['code'] = $("#gs-search-advanced").find('input[name="code"]').val();
    }
    LoginLog.table.refresh({query: queryData});
};

/**
 * 重置登录日志列表
 */
LoginLog.resetSearch = function(){
    LoginLog.table.setQueryParams({});
    LoginLog.table.refresh();
};

$(function () {
    var defaultColunms = LoginLog.initColumn();
    var table = new GSTable(LoginLog.id, "/loginLog/list", defaultColunms);
    table.setPaginationType("client");
    LoginLog.table = table.init();
});
