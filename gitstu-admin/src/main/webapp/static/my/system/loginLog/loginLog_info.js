/**
 * 初始化登录日志详情对话框
 */
var LoginLogInfoDlg = {
    loginLogInfoData : {}
};

/**
 * 清除数据
 */
LoginLogInfoDlg.clearData = function() {
    this.loginLogInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LoginLogInfoDlg.set = function(key, val) {
    this.loginLogInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LoginLogInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LoginLogInfoDlg.close = function() {
    parent.layer.close(window.parent.LoginLog.layerIndex);
}

/**
 * 收集数据
 */
LoginLogInfoDlg.collectData = function() {
    this
    .set('id')
    .set('logname')
    .set('userid')
    .set('createtime')
    .set('succeed')
    .set('message')
    .set('ip');
}

/**
 * 提交添加
 */
LoginLogInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(GsUtils.ctxPath + "/loginLog/add", function(data){
        GsUtils.success("添加成功!");
        window.parent.LoginLog.table.refresh();
        LoginLogInfoDlg.close();
    },function(data){
        GsUtils.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.loginLogInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LoginLogInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(GsUtils.ctxPath + "/loginLog/update", function(data){
        GsUtils.success("修改成功!");
        window.parent.LoginLog.table.refresh();
        LoginLogInfoDlg.close();
    },function(data){
        GsUtils.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.loginLogInfoData);
    ajax.start();
}

$(function() {

});
