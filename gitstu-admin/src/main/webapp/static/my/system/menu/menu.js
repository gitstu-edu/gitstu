/**
 * 菜单管理管理初始化
 */
var Menu = {
    id: "MenuTable",	//表格id
    seItem: null,		//选中的条目
    table: null
};

/**
 * 初始化表格的列
 */
Menu.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '主键id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '菜单编号', field: 'code', visible: true, align: 'center', valign: 'middle'},
            {title: '菜单父编号', field: 'pcode', visible: true, align: 'center', valign: 'middle'},
            {title: '当前菜单的所有父菜单编号', field: 'pcodes', visible: true, align: 'center', valign: 'middle'},
            {title: '菜单名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '菜单图标', field: 'icon', visible: true, align: 'center', valign: 'middle'},
            {title: 'url地址', field: 'url', visible: true, align: 'center', valign: 'middle'},
            {title: '菜单排序号', field: 'num', visible: true, align: 'center', valign: 'middle'},
            {title: '菜单层级', field: 'levels', visible: true, align: 'center', valign: 'middle'},
            {title: '是否是菜单（1：是  0：不是）', field: 'ismenu', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'tips', visible: true, align: 'center', valign: 'middle'},
            {title: '菜单状态 :  1:启用   0:不启用', field: 'status', visible: true, align: 'center', valign: 'middle'},
            {title: '是否打开:    1:打开   0:不打开', field: 'isopen', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Menu.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        GsUtils.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Menu.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加菜单管理
 */
Menu.add = function () {
    location.href = GsUtils.ctxPath + '/menu/menu_add';
};

/**
 * 打开查看菜单管理详情
 */
Menu.edit = function () {
    if (this.check()) {
        location.href = GsUtils.ctxPath + '/menu/menu_edit/' + Menu.seItem.id;
    }
};

/**
 * 删除菜单管理
 */
Menu.delete = function () {
    if (this.check()) {
        var operation = function(){
            var ajax = new $ax(GsUtils.ctxPath + "/menu/delete", function (data) {
                GsUtils.success("删除成功!");
                Menu.table.refresh();
            }, function (data) {
                GsUtils.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("menuId",Menu.seItem.id);
            ajax.start();
        };

        GsUtils.confirm("是否刪除菜单管理 " + Menu.seItem.name + "?", operation);
    }
};

/**
 * 查询菜单管理列表
 */
Menu.search = function () {
    var queryData = {};
    if(GsUtils.cbChecked($('#gs-search-type'))){
        queryData['gsCondition'] = $("#gs-search-easy").find('input[name="gsCondition"]').val();
    }else{
        queryData['name'] = $("#gs-search-advanced").find('input[name="name"]').val();
        queryData['code'] = $("#gs-search-advanced").find('input[name="code"]').val();
    }
    Menu.table.refresh({query: queryData});
};

/**
 * 重置菜单管理列表
 */
Menu.resetSearch = function(){
    Menu.table.setQueryParams({});
    Menu.table.refresh();
};

$(function () {
    var defaultColunms = Menu.initColumn();
    var table = new GSTable(Menu.id, "/menu/list", defaultColunms);
    table.setPaginationType("client");
    Menu.table = table.init();
});
