/**
 * 初始化菜单管理详情对话框
 */
var MenuInfoDlg = {
    menuInfoData : {}
};

/**
 * 清除数据
 */
MenuInfoDlg.clearData = function() {
    this.menuInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MenuInfoDlg.set = function(key, val) {
    this.menuInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MenuInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
MenuInfoDlg.close = function() {
    parent.layer.close(window.parent.Menu.layerIndex);
}

/**
 * 收集数据
 */
MenuInfoDlg.collectData = function() {
    this
    .set('id')
    .set('code')
    .set('pcode')
    .set('pcodes')
    .set('name')
    .set('icon')
    .set('url')
    .set('num')
    .set('levels')
    .set('ismenu')
    .set('tips')
    .set('status')
    .set('isopen');
}

/**
 * 提交添加
 */
MenuInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(GsUtils.ctxPath + "/menu/add", function(data){
        GsUtils.success("添加成功!");
        window.parent.Menu.table.refresh();
        MenuInfoDlg.close();
    },function(data){
        GsUtils.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.menuInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
MenuInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(GsUtils.ctxPath + "/menu/update", function(data){
        GsUtils.success("修改成功!");
        window.parent.Menu.table.refresh();
        MenuInfoDlg.close();
    },function(data){
        GsUtils.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.menuInfoData);
    ajax.start();
}

$(function() {

});
