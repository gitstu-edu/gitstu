/**
 * 通知管理管理初始化
 */
var Notice = {
    id: "NoticeTable",	//表格id
    seItem: null,		//选中的条目
    table: null
};

/**
 * 初始化表格的列
 */
Notice.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '主键', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '标题', field: 'title', visible: true, align: 'center', valign: 'middle'},
            {title: '类型', field: 'type', visible: true, align: 'center', valign: 'middle'},
            {title: '内容', field: 'content', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'},
            {title: '创建人', field: 'creater', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Notice.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        GsUtils.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Notice.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加通知管理
 */
Notice.add = function () {
    location.href = GsUtils.ctxPath + '/notice/notice_add';
};

/**
 * 打开查看通知管理详情
 */
Notice.edit = function () {
    if (this.check()) {
        location.href = GsUtils.ctxPath + '/notice/notice_edit/' + Notice.seItem.id;
    }
};

/**
 * 删除通知管理
 */
Notice.delete = function () {
    if (this.check()) {
        var operation = function(){
            var ajax = new $ax(GsUtils.ctxPath + "/notice/delete", function (data) {
                GsUtils.success("删除成功!");
                Notice.table.refresh();
            }, function (data) {
                GsUtils.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("noticeId",Notice.seItem.id);
            ajax.start();
        };

        GsUtils.confirm("是否刪除通知管理 " + Notice.seItem.name + "?", operation);
    }
};

/**
 * 查询通知管理列表
 */
Notice.search = function () {
    var queryData = {};
    if(GsUtils.cbChecked($('#gs-search-type'))){
        queryData['gsCondition'] = $("#gs-search-easy").find('input[name="gsCondition"]').val();
    }else{
        queryData['name'] = $("#gs-search-advanced").find('input[name="name"]').val();
        queryData['code'] = $("#gs-search-advanced").find('input[name="code"]').val();
    }
    Notice.table.refresh({query: queryData});
};

/**
 * 重置通知管理列表
 */
Notice.resetSearch = function(){
    Notice.table.setQueryParams({});
    Notice.table.refresh();
};

$(function () {
    var defaultColunms = Notice.initColumn();
    var table = new GSTable(Notice.id, "/notice/list", defaultColunms);
    table.setPaginationType("client");
    Notice.table = table.init();
});
