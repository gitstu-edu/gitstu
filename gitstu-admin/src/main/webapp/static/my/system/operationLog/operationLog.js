/**
 * 业务日志管理初始化
 */
var OperationLog = {
    id: "OperationLogTable",	//表格id
    seItem: null,		//选中的条目
    table: null
};

/**
 * 初始化表格的列
 */
OperationLog.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '主键', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '日志类型', field: 'logtype', visible: true, align: 'center', valign: 'middle'},
            {title: '日志名称', field: 'logname', visible: true, align: 'center', valign: 'middle'},
            {title: '用户id', field: 'userid', visible: true, align: 'center', valign: 'middle'},
            {title: '类名称', field: 'classname', visible: true, align: 'center', valign: 'middle'},
            {title: '方法名称', field: 'method', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'},
            {title: '是否成功', field: 'succeed', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'message', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
OperationLog.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        GsUtils.info("请先选中表格中的某一记录！");
        return false;
    }else{
        OperationLog.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加业务日志
 */
OperationLog.add = function () {
    location.href = GsUtils.ctxPath + '/operationLog/operationLog_add';
};

/**
 * 打开查看业务日志详情
 */
OperationLog.update = function () {
    if (this.check()) {
        location.href = GsUtils.ctxPath + '/operationLog/operationLog_update/' + OperationLog.seItem.id;
    }
};

/**
 * 删除业务日志
 */
OperationLog.delete = function () {
    if (this.check()) {
        var operation = function(){
            var ajax = new $ax(GsUtils.ctxPath + "/operationLog/delete", function (data) {
                GsUtils.success("删除成功!");
                OperationLog.table.refresh();
            }, function (data) {
                GsUtils.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("operationLogId",OperationLog.seItem.id);
            ajax.start();
        };

        GsUtils.confirm("是否刪除业务日志 " + OperationLog.seItem.name + "?", operation);
    }
};

/**
 * 查询业务日志列表
 */
OperationLog.search = function () {
    var queryData = {};
    if(GsUtils.cbChecked($('#gs-search-type'))){
        queryData['gsCondition'] = $("#gs-search-easy").find('input[name="gsCondition"]').val();
    }else{
        queryData['name'] = $("#gs-search-advanced").find('input[name="name"]').val();
        queryData['code'] = $("#gs-search-advanced").find('input[name="code"]').val();
    }
    OperationLog.table.refresh({query: queryData});
};

/**
 * 重置业务日志列表
 */
OperationLog.resetSearch = function(){
    OperationLog.table.setQueryParams({});
    OperationLog.table.refresh();
};

$(function () {
    var defaultColunms = OperationLog.initColumn();
    var table = new GSTable(OperationLog.id, "/operationLog/list", defaultColunms);
    table.setPaginationType("client");
    OperationLog.table = table.init();
});
