/**
 * 角色管理管理初始化
 */
var Role = {
    id: "RoleTable",	//表格id
    seItem: null,		//选中的条目
    table: null
};

/**
 * 初始化表格的列
 */
Role.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '主键id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '序号', field: 'num', visible: true, align: 'center', valign: 'middle'},
            {title: '父角色id', field: 'pid', visible: true, align: 'center', valign: 'middle'},
            {title: '角色名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '部门名称', field: 'deptid', visible: true, align: 'center', valign: 'middle'},
            {title: '提示', field: 'tips', visible: true, align: 'center', valign: 'middle'},
            {title: '保留字段(暂时没用）', field: 'version', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Role.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        GsUtils.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Role.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加角色管理
 */
Role.add = function () {
    location.href = GsUtils.ctxPath + '/role/role_add';
};

/**
 * 打开查看角色管理详情
 */
Role.edit = function () {
    if (this.check()) {
        location.href = GsUtils.ctxPath + '/role/role_edit/' + Role.seItem.id;
    }
};

/**
 * 删除角色管理
 */
Role.delete = function () {
    if (this.check()) {
        var operation = function(){
            var ajax = new $ax(GsUtils.ctxPath + "/role/delete", function (data) {
                GsUtils.success("删除成功!");
                Role.table.refresh();
            }, function (data) {
                GsUtils.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("roleId",Role.seItem.id);
            ajax.start();
        };

        GsUtils.confirm("是否刪除角色管理 " + Role.seItem.name + "?", operation);
    }
};

/**
 * 查询角色管理列表
 */
Role.search = function () {
    var queryData = {};
    if(GsUtils.cbChecked($('#gs-search-type'))){
        queryData['gsCondition'] = $("#gs-search-easy").find('input[name="gsCondition"]').val();
    }else{
        queryData['name'] = $("#gs-search-advanced").find('input[name="name"]').val();
        queryData['code'] = $("#gs-search-advanced").find('input[name="code"]').val();
    }
    Role.table.refresh({query: queryData});
};

/**
 * 重置角色管理列表
 */
Role.resetSearch = function(){
    Role.table.setQueryParams({});
    Role.table.refresh();
};

$(function () {
    var defaultColunms = Role.initColumn();
    var table = new GSTable(Role.id, "/role/list", defaultColunms);
    table.setPaginationType("client");
    Role.table = table.init();
});
