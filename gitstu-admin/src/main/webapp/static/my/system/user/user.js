/**
 * 用户管理管理初始化
 */
var User = {
    id: "UserTable",	//表格id
    seItem: null,		//选中的条目
    table: null
};

/**
 * 初始化表格的列
 */
User.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '主键id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '头像', field: 'avatar', visible: true, align: 'center', valign: 'middle'},
            {title: '账号', field: 'account', visible: true, align: 'center', valign: 'middle'},
            {title: '密码', field: 'password', visible: true, align: 'center', valign: 'middle'},
            {title: 'md5密码盐', field: 'salt', visible: true, align: 'center', valign: 'middle'},
            {title: '名字', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '生日', field: 'birthday', visible: true, align: 'center', valign: 'middle'},
            {title: '性别（1：男 2：女）', field: 'sex', visible: true, align: 'center', valign: 'middle'},
            {title: '电子邮件', field: 'email', visible: true, align: 'center', valign: 'middle'},
            {title: '电话', field: 'phone', visible: true, align: 'center', valign: 'middle'},
            {title: '角色id', field: 'roleid', visible: true, align: 'center', valign: 'middle'},
            {title: '部门id', field: 'deptid', visible: true, align: 'center', valign: 'middle'},
            {title: '状态(1：启用  2：冻结  3：删除）', field: 'status', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'},
            {title: '保留字段', field: 'version', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
User.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        GsUtils.info("请先选中表格中的某一记录！");
        return false;
    }else{
        User.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加用户管理
 */
User.add = function () {
    location.href = GsUtils.ctxPath + '/user/user_add';
};

/**
 * 打开查看用户管理详情
 */
User.update = function () {
    if (this.check()) {
        location.href = GsUtils.ctxPath + '/user/user_edit/' + User.seItem.id;
    }
};

/**
 * 角色分配
 */
User.roleAssign = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '角色分配',
            area: ['300px', '400px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: GsUtils.ctxPath + '/user/role_assign/' + User.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除用户管理
 */
User.delete = function () {
    if (this.check()) {
        var operation = function(){
            var ajax = new $ax(GsUtils.ctxPath + "/user/delete", function (data) {
                GsUtils.success("删除成功!");
                User.table.refresh();
            }, function (data) {
                GsUtils.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("userId",User.seItem.id);
            ajax.start();
        };

        GsUtils.confirm("是否刪除用户" + User.seItem.name + "?", operation);
    }
};

/**
 * 冻结用户账户
 * @param userId
 */
User.freezeAccount = function () {
    if (this.check()) {
        var userId = User.seItem.id;
        var ajax = new $ax(GsUtils.ctxPath + "/user/freeze", function (data) {
            GsUtils.success("冻结成功!");
            User.table.refresh();
        }, function (data) {
            GsUtils.error("冻结失败!" + data.responseJSON.message + "!");
        });
        ajax.set("userId", userId);
        ajax.start();
    }
};

/**
 * 解除冻结用户账户
 * @param userId
 */
User.unfreeze = function () {
    if (this.check()) {
        var userId = User.seItem.id;
        var ajax = new $ax(GsUtils.ctxPath + "/user/unfreeze", function (data) {
            GsUtils.success("解除冻结成功!");
            User.table.refresh();
        }, function (data) {
            GsUtils.error("解除冻结失败!");
        });
        ajax.set("userId", userId);
        ajax.start();
    }
}

/**
 * 重置密码
 */
User.resetPwd = function () {
    if (this.check()) {
        var userId = this.seItem.id;
        parent.layer.confirm('是否重置密码为111111？', {
            btn: ['确定', '取消'],
            shade: false //不显示遮罩
        }, function () {
            var ajax = new $ax(GsUtils.ctxPath + "/user/reset", function (data) {
                GsUtils.success("重置密码成功!");
            }, function (data) {
                GsUtils.error("重置密码失败!");
            });
            ajax.set("userId", userId);
            ajax.start();
        });
    }
};

/**
 * 查询用户管理列表
 */
User.search = function () {
    var queryData = {};

    queryData['deptid'] = User.deptid;
    if(GsUtils.cbChecked($('#gs-search-type'))){
        queryData['gsCondition'] = $("#gs-search-easy").find('input[name="gsCondition"]').val();
    }else{
        queryData['name'] = $("#gs-search-advanced").find('input[name="name"]').val();
        queryData['code'] = $("#gs-search-advanced").find('input[name="code"]').val();
    }
    User.table.refresh({query: queryData});
};

/**
 * 重置用户管理列表
 */
User.resetSearch = function(){
    var queryData = {};
    queryData['deptid'] = MgrUser.deptid;
    User.table.setQueryParams(queryData);
    User.table.refresh();
};

/**
 * 点击树
 * @param e
 * @param treeId
 * @param treeNode
 */
User.onClickDept = function (e, treeId, treeNode) {
    User.deptid = treeNode.id;
    User.search();
};

$(function () {
    //初始化表格
    var defaultColunms = User.initColumn();
    var table = new GSTable(User.id, "/user/list", defaultColunms);
    table.setPaginationType("client");
    User.table = table.init();

    //初始化树
    var ztree = new $ZTree("deptTree", "/dept/tree");
    ztree.bindOnClick(User.onClickDept);
    ztree.init();
});
