package com.lg.gitstu;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * REST Web程序启动类
 *
 * @author 迅白旺旺
 * @date 2017年9月29日09:00:42
 */
public class ApiServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(ApiApp.class);
    }

}
