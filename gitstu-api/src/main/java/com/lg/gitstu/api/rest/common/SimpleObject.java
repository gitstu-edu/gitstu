package com.lg.gitstu.api.rest.common;

import lombok.Data;

/**
 * 测试用的
 *
 * @author 迅白旺旺
 * @date 2017-08-25 16:47
 */
@Data
public class SimpleObject {

    private String user;

    private String name;

    private String tips;

    private Integer age;
}
