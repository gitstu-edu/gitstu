package com.lg.gitstu.api.rest.common.exception;

import com.lg.gitstu.core.exception.ServiceExceptionEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * 所有业务异常的枚举
 *
 * @author 迅白旺旺
 * @date 2016年11月12日 下午5:04:51
 */
public enum BizExceptionEnum implements ServiceExceptionEnum {

    /**
     * token异常
     */
    TOKEN_EXPIRED(700, "token过期"),
    TOKEN_ERROR(700, "token验证失败"),

    /**
     * 签名异常
     */
    SIGN_ERROR(700, "签名验证失败"),

    /**
     * 其他
     */
    AUTH_REQUEST_ERROR(400, "账号密码错误");

    BizExceptionEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Getter
    @Setter
    private Integer code;

    @Getter
    @Setter
    private String message;
}
