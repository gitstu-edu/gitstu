package com.lg.gitstu.api.rest.common.persistence.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lg.gitstu.api.rest.common.persistence.model.User;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author 迅白旺旺
 * @since 2017-08-23
 */
public interface UserMapper extends BaseMapper<User> {

}