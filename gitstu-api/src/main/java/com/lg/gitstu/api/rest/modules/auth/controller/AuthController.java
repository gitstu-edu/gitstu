package com.lg.gitstu.api.rest.modules.auth.controller;

import com.lg.gitstu.api.rest.common.exception.BizExceptionEnum;
import com.lg.gitstu.api.rest.modules.auth.controller.dto.AuthRequest;
import com.lg.gitstu.api.rest.modules.auth.controller.dto.AuthResponse;
import com.lg.gitstu.api.rest.modules.auth.util.JwtTokenUtil;
import com.lg.gitstu.api.rest.modules.auth.validator.IReqValidator;
import com.lg.gitstu.core.exception.GitStuException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 请求验证的
 *
 * @author 迅白旺旺
 * @Date 2017/8/24 14:22
 */
@RestController
public class AuthController {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Resource(name = "simpleValidator")
    private IReqValidator reqValidator;

    @RequestMapping(value = "${jwt.auth-path}")
    public ResponseEntity<?> createAuthenticationToken(AuthRequest authRequest) {

        boolean validate = reqValidator.validate(authRequest);

        if (validate) {
            final String randomKey = jwtTokenUtil.getRandomKey();
            final String token = jwtTokenUtil.generateToken(authRequest.getUserName(), randomKey);
            return ResponseEntity.ok(new AuthResponse(token, randomKey));
        } else {
            throw new GitStuException(BizExceptionEnum.AUTH_REQUEST_ERROR);
        }
    }
}
