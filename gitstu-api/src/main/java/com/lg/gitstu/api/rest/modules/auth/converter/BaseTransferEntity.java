package com.lg.gitstu.api.rest.modules.auth.converter;

import lombok.Data;

/**
 * 基础的传输bean
 *
 * @author 迅白旺旺
 * @date 2017-08-25 15:52
 */
@Data
public class BaseTransferEntity {

    private String object; //base64编码的json字符串

    private String sign;   //签名
}
