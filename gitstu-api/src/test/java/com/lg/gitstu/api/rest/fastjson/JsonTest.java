package com.lg.gitstu.api.rest.fastjson;

import com.alibaba.fastjson.JSON;
import com.lg.gitstu.api.rest.common.SimpleObject;
import com.lg.gitstu.api.rest.modules.auth.converter.BaseTransferEntity;
import com.lg.gitstu.core.util.MD5Util;

/**
 * json测试
 *
 * @author 迅白旺旺
 * @date 2017-08-25 16:11
 */


public class JsonTest {

    public static void main(String[] args) {
        String randomKey = "1xm7hw";

        BaseTransferEntity baseTransferEntity = new BaseTransferEntity();
        SimpleObject simpleObject = new SimpleObject();
        simpleObject.setUser("fsn");
        baseTransferEntity.setObject("123123");

        String json = JSON.toJSONString(simpleObject);

        //md5签名
        String encrypt = MD5Util.encrypt(json + randomKey);
        baseTransferEntity.setSign(encrypt);

        System.out.println(JSON.toJSONString(baseTransferEntity));
    }
}
