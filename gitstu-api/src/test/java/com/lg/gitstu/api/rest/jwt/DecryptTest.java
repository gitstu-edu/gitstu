package com.lg.gitstu.api.rest.jwt;

import com.alibaba.fastjson.JSON;
import com.lg.gitstu.api.rest.common.SimpleObject;
import com.lg.gitstu.api.rest.modules.auth.converter.BaseTransferEntity;
import com.lg.gitstu.api.rest.modules.auth.security.impl.Base64SecurityAction;
import com.lg.gitstu.core.util.MD5Util;

/**
 * jwt测试
 *
 * @author 迅白旺旺
 * @date 2017-08-21 16:34
 */
public class DecryptTest {

    public static void main(String[] args) {

        String salt = "0iqwhi";

        SimpleObject simpleObject = new SimpleObject();
        simpleObject.setUser("迅白旺旺");
        simpleObject.setAge(12);
        simpleObject.setName("ffff");
        simpleObject.setTips("code");

        String jsonString = JSON.toJSONString(simpleObject);
        String encode = new Base64SecurityAction().doAction(jsonString);
        String md5 = MD5Util.encrypt(encode + salt);

        BaseTransferEntity baseTransferEntity = new BaseTransferEntity();
        baseTransferEntity.setObject(encode);
        baseTransferEntity.setSign(md5);

        System.out.println(JSON.toJSONString(baseTransferEntity));
    }
}
