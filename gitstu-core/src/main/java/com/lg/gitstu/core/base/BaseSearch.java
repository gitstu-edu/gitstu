package com.lg.gitstu.core.base;

import lombok.Data;

/**
 * 条件检索
 */
@Data
public abstract class BaseSearch {
    /**
     * 简易搜索任意条件
     */
    protected String gsCondition;
}
