package com.lg.gitstu.core.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * 封装异常
 *
 * @author 迅白旺旺
 * @Date 2017/12/28 下午10:32
 */
@Getter
@Setter
public class GitStuException extends RuntimeException {

    private Integer code;

    private String message;

    public GitStuException(ServiceExceptionEnum serviceExceptionEnum) {
        this.code = serviceExceptionEnum.getCode();
        this.message = serviceExceptionEnum.getMessage();
    }
}
