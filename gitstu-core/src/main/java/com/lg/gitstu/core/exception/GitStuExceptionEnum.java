package com.lg.gitstu.core.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * gitstu异常枚举
 *
 * @author 迅白旺旺
 * @Date 2017/12/28 下午10:33
 */
public enum GitStuExceptionEnum implements ServiceExceptionEnum{

	/**
	 * 其他
	 */
	WRITE_ERROR(500,"渲染界面错误"),

	/**
	 * 文件上传
	 */
	FILE_READING_ERROR(400,"FILE_READING_ERROR!"),
	FILE_NOT_FOUND(400,"FILE_NOT_FOUND!"),

	/**
	 * 错误的请求
	 */
	REQUEST_NULL(400, "请求有错误"),
	SERVER_ERROR(500, "服务器异常");

	GitStuExceptionEnum(int code, String message) {
		this.code = code;
		this.message = message;
	}

	@Getter
	@Setter
	private Integer code;

	@Getter
	@Setter
	private String message;
}
