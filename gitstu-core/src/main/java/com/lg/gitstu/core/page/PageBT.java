package com.lg.gitstu.core.page;

import lombok.Data;

/**
 * 分页参数类（for BootStrap Table）
 *
 * @author 迅白旺旺
 * @date 2017年1月21日 下午2:21:35
 */
@Data
public class PageBT {

    private int limit;          // 每页显示个数

    private int offset;         // 查询的偏移量（查询的页数 = offset/limit + 1）

    private String order;       // 排序方式

    public PageBT(int limit, int offset) {
        super();
        this.limit = limit;
        this.offset = offset;
    }

}
