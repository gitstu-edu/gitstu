package com.lg.gitstu.gen.engine.config;

import com.lg.gitstu.core.util.ToolUtil;
import lombok.Data;

/**
 * 全局配置
 *
 * @author 迅白旺旺
 * @date 2017-05-08 20:21
 */
@Data
public class ContextConfig {

    private String templatePrefixPath = "gitstuTemplate/advanced";
    private String projectPath = "D:\\ideaSpace\\gitstu";//模板输出的项目目录
    private String bizChName;   //业务名称
    private String bizEnName;   //业务英文名称
    private String bizEnBigName;//业务英文名称(大写)
    private String moduleName = "system";  //模块名称

    private String proPackage = "com.lg.gitstu.admin";
    private String coreBasePackage = "com.lg.gitstu.core";
    private String searchPackageName = "com.lg.gitstu.admin.common.search";        //model的包名
    private String modelPackageName = "com.lg.gitstu.admin.common.model";        //model的包名
    private String modelMapperPackageName = "com.lg.gitstu.admin.modules.system.dao";    //model的dao
    private String entityName;              //实体的名称

    private Boolean controllerSwitch = true;    //是否生成控制器代码开关
    private Boolean indexPageSwitch = true;     //主页
    private Boolean addPageSwitch = true;       //添加页面
    private Boolean editPageSwitch = true;      //编辑页面
    private Boolean jsSwitch = true;            //js
    private Boolean infoJsSwitch = true;        //详情页面js
    private Boolean daoSwitch = true;           //dao
    private Boolean serviceSwitch = true;       //service
    private Boolean entitySwitch = true;        //生成实体的开关
    private Boolean sqlSwitch = true;           //生成sql的开关

    public void init() {
        if (entityName == null) {
            entityName = bizEnBigName;
        }
        modelPackageName = proPackage + ".common.model";
        searchPackageName = proPackage + ".common.search";
        modelMapperPackageName = proPackage + ".modules.system.dao";
    }

    public void setBizEnName(String bizEnName) {
        this.bizEnName = bizEnName;
        this.bizEnBigName = ToolUtil.firstLetterToUpper(this.bizEnName);
    }

}
