package com.lg.gitstu.gen.engine.config;

import lombok.Getter;
import lombok.Setter;

/**
 * Dao模板生成的配置
 *
 * @author 迅白旺旺
 * @date 2017-05-07 22:12
 */
@Getter
@Setter
public class DaoConfig {

    private ContextConfig contextConfig;

    private String daoPathTemplate;
    private String xmlPathTemplate;

    private String packageName;

    public void init() {
        this.daoPathTemplate = "\\src\\main\\java\\" + contextConfig.getProPackage().replaceAll("\\.", "\\\\") + "\\modules\\" + contextConfig.getModuleName() + "\\dao\\{}Dao.java";
        this.xmlPathTemplate = "\\src\\main\\java\\" + contextConfig.getProPackage().replaceAll("\\.", "\\\\") + "\\modules\\" + contextConfig.getModuleName() + "\\dao\\mapping\\{}Dao.xml";
        this.packageName = contextConfig.getProPackage() + ".modules." + contextConfig.getModuleName() + ".dao";
    }
}
